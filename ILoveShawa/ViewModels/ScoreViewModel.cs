﻿using System.ComponentModel.DataAnnotations;

namespace ILoveShawa.ViewModels
{
    public class ScoreViewModel
    {
        [Required]
        public int ShopId { get; set; }

        [Required]
        public int ScoreCount { get; set; }

        [Required]
        public double AverageScore { get; set; }

        [Required]
        public int? UserScore { get; set; }
    }
}