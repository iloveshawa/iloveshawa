﻿using System.ComponentModel.DataAnnotations;

namespace ILoveShawa.ViewModels
{
    public class ShawaShopViewModel
    {
        [Required]
        [Display(Name = "Название заведения")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Долгота")]
        public double Longitude { get; set; }

        [Required]
        [Display(Name = "Широта")]
        public double Latitude { get; set; }

        [Display(Name = "Ссылка на аватарку")]
        public string ImageUrl { get; set; }

        [Required]
        [Display(Name = "Адрес")]
        public string Address { get; set; }
    }
}