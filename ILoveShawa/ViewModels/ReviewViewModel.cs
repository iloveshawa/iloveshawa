﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ILoveShawa.ViewModels
{
    public class ReviewViewModel
    {
        [Required]
        public int ShopId { get; set; }

        [Required]
        [Display(Name = "Текст рецензии")]
        public string Text { get; set; }

        [Required]
        [Display(Name = "Положительная рецензия")]
        public bool Positive { get; set; }
    }
}