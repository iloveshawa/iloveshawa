﻿using System.ComponentModel.DataAnnotations;

namespace ILoveShawa.ViewModels
{
    public class LikeViewModel
    {
        [Required]
        public int ReviewId { get; set; }

        [Required]
        public int Count { get; set; }

        [Required]
        public bool Liked { get; set; }
    }
}