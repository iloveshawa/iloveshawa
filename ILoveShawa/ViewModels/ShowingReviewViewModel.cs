﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ILoveShawa.Models;

namespace ILoveShawa.ViewModels
{
    public class ShowingReviewViewModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string AvatarUrl { get; set; }
        public Review Review { get; set; }
    }
}