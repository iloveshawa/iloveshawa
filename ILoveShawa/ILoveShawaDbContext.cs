﻿using System.Data.Entity;
using ILoveShawa.Models;

namespace ILoveShawa
{
    public class ILoveShawaDbContext : DbContext

    {
        public ILoveShawaDbContext() : base("ILoveShawaDbContextConnectionString")
        {
            Database.SetInitializer(new ILoveShawaDbInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Image>()
                .HasRequired<ShawaShop>(image => image.ShawaShop)
                .WithMany(shop => shop.Images)
                .HasForeignKey(image => image.ShopId);
            modelBuilder.Entity<Like>()
                .HasRequired<User>(like => like.User)
                .WithMany(user => user.Likes)
                .HasForeignKey(like => like.UserId);
            modelBuilder.Entity<Review>()
                .HasRequired<ShawaShop>(review => review.ShawaShop)
                .WithMany(shop => shop.Reviews)
                .HasForeignKey(review => review.ShopId);
            modelBuilder.Entity<Score>()
                .HasRequired<ShawaShop>(score => score.ShawaShop)
                .WithMany(shop => shop.Scores)
                .HasForeignKey(score => score.ShopId);
            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<Image> Images { get; set; }

        public virtual DbSet<Like> Likes { get; set; }

        public virtual DbSet<Review> Reviews { get; set; }

        public virtual DbSet<Score> Scores { get; set; }

        public virtual DbSet<ShawaShop> ShawaShops { get; set; }

        public virtual DbSet<User> Users { get; set; }
    }

    public class ILoveShawaDbInitializer : CreateDatabaseIfNotExists<ILoveShawaDbContext>
    {
        protected override void Seed(ILoveShawaDbContext context)
        {
            // create 3 students to seed the database
            //context.Students.Add(new Student { ID = 1, FirstName = "Mark", LastName = "Richards", EnrollmentDate = DateTime.Now });
            //context.Students.Add(new Student { ID = 2, FirstName = "Paula", LastName = "Allen", EnrollmentDate = DateTime.Now });
            //context.Students.Add(new Student { ID = 3, FirstName = "Tom", LastName = "Hoover", EnrollmentDate = DateTime.Now });
            base.Seed(context);
        }
    }

}