﻿var myMap;
var shopItems = { dom: [], placemarks: [] };

function addMap() {
    myMap = new ymaps.Map("map", {
        center: [59.92095082, 30.36707118],
        zoom: 11,
        controls: []
    });

    var zoomControl = new ymaps.control.ZoomControl({
        options: {
            size: "small",
            position: {
                top: 10,
                left: 10
            }
        }
    });
    myMap.controls.add(zoomControl);
}

function initMap() {
    addMap();

    if (typeof shopsData !== "undefined") {
        for (var i in shopsData) {
            var shop = shopsData[i];
            shopItems.dom[i] = $(".shop-item[shop-id=" + shop.Id + "]").first();
            myPlacemark = new ymaps.Placemark([shop.Latitude, shop.Longitude],
                { iconContent: i, balloonContentHeader: "<a href=\"" + shop.Url + "\">" + shop.Name + "</a>" });
            shopItems.placemarks[i] = myPlacemark;
            myPlacemark.events
                .add("mouseenter", mouseenterPlacemark)
                .add("mouseleave", mouseleavePlacemark)
                .add("click", clickPlacemark)
                .add("balloonclose", balloonclosePlacemark);
            myMap.geoObjects.add(myPlacemark);
            shopItems.dom[i].hover(mouseenterShopDomItem, mouseleaveShopDomItem);
        }
    }
    else if (typeof singleShopData !== "undefined") {
        myMap.setCenter([singleShopData.Latitude, singleShopData.Longitude], 16);
        myPlacemark = new ymaps.Placemark([singleShopData.Latitude, singleShopData.Longitude],
                { balloonContentHeader: "<a href=\"" + singleShopData.Url + "\">" + singleShopData.Name + "</a>" });
        myMap.geoObjects.add(myPlacemark);
    }
}

function mouseenterPlacemark(e) {
    var index = shopItems.placemarks.indexOf(e.get("target"));
    shopItems.placemarks[index].options.set("preset", "islands#orangeStretchyIcon");
    shopItems.placemarks[index].properties.set("iconContent", shopsData[index].Name);
    shopItems.dom[index].addClass("hover");
}

function mouseleavePlacemark(e) {
    var index = shopItems.placemarks.indexOf(e.get("target"));
    e.get("target").options.unset("preset");
    shopItems.placemarks[index].properties.set("iconContent", index);
    shopItems.dom[index].removeClass("hover");
}

function clickPlacemark(e) {
    var index = shopItems.placemarks.indexOf(e.get("target"));
    shopItems.placemarks[index].options.set("preset", "islands#orangeStretchyIcon");
    shopItems.placemarks[index].properties.set("iconContent", shopsData[index].Name);
    shopItems.dom[index].addClass("selected");
    shopItems.dom[index].removeClass("hover");
    var offset = shopItems.dom[index].offset().top;
    var visible_area_start = $(window).scrollTop() + 60;
    var visible_area_end = visible_area_start + window.innerHeight;
    if (offset < visible_area_start || offset > visible_area_end) {
        $('html,body').animate({ scrollTop: offset - window.innerHeight / 3 }, 400);
    }
}

function balloonclosePlacemark(e) {
    var index = shopItems.placemarks.indexOf(e.get("target"));
    shopItems.placemarks[index].options.unset("preset");
    shopItems.placemarks[index].properties.set("iconContent", index);
    shopItems.dom[index].removeClass("selected");
}

function mouseenterShopDomItem(e) {
    index = shopItems.dom.findIndex(function (item) { return item[0] === e.currentTarget; });
    shopItems.placemarks[index].options.set("preset", "islands#orangeStretchyIcon");
    shopItems.placemarks[index].properties.set("iconContent", shopsData[index].Name);
}

function mouseleaveShopDomItem(e) {
    index = shopItems.dom.findIndex(function (item) { return item[0] === e.currentTarget; });
    shopItems.placemarks[index].options.unset("preset");
    shopItems.placemarks[index].properties.set("iconContent", index);

}

function initAddShawaShop() {
    addMap();
    var placeMark = new ymaps.Placemark();
    placeMark.options.set('preset', 'islands#greenStretchyIcon');
    placeMark.events.add('geometrychange', function (event) {
        var coords = placeMark.geometry.getCoordinates();
        document.getElementById('shop-latitude').value = coords[0];
        document.getElementById('shop-longitude').value = coords[1];
    });
    myMap.geoObjects.add(placeMark);

    var suggest = new ymaps.SuggestView('shop-address');
    suggest.events.add('select', function (event) {
        var address = event.get('item');
        ymaps.geocode(address.value, { results: 1 }).then(function (result) {
            var coords = result.geoObjects.get(0).geometry.getCoordinates();
            placeMark.geometry.setCoordinates(coords);
            myMap.setCenter(coords);
        })
    })

    myMap.events.add('click', function (e) {
        var coords = e.get('coords');
        ymaps.geocode(coords, { results: 1 }).then(function (result) {
            var address = result.geoObjects.get(0).properties.get('text');
            document.getElementById('shop-address').value = address;
            placeMark.geometry.setCoordinates(coords);
        })
    });
}

$(document).ready(function () {
    $("#show-and-add-score").on("change", ".rating-radio", function () {
        $("#score-form").submit();
    });
    $("#show-and-add-score.unauthorized").on("click", ".rating-label", function () {
        $("#unauthorized-modal").modal("show");
    });
    $('[data-toggle="tooltip"]').tooltip();

    $('.photo-list img').on('click', function () {
        var src = $(this).attr('src');
        var img = '<img src="' + src + '" class="img-responsive photo-modal"/>';
        $('#myModal').modal();
        $('#myModal').on('shown.bs.modal', function () {
            $('#myModal .modal-body').html(img);
        });
        $('#myModal').on('hidden.bs.modal', function () {
            $('#myModal .modal-body').html('');
        });
    });

    $("#show-and-add-score").on("change", ".rating-radio", function() {
        $("#score-form").submit();
    });
});
