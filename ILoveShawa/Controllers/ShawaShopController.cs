﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using ILoveShawa.Models;
using ILoveShawa.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace ILoveShawa.Controllers
{
    [Authorize]
    public class ShawaShopController : Controller
    {
        public ShawaShopController(ILoveShawaDbContext dbContext)
        {
            db = dbContext;
        }

        private readonly ILoveShawaDbContext db;

        [AllowAnonymous]
        public ActionResult Index()
        {
            var showindShops = db.ShawaShops.AsQueryable().ToList();
            return View(showindShops);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Get(int id = -1)
        {
            var shawaShop = db.ShawaShops.Get(id);
            if (shawaShop != null)
            {
                return View("SingleShawaShop", shawaShop);
            }
            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(ShawaShopViewModel model)
        {
            if (ModelState.IsValid)
            {
                var shop = new ShawaShop()
                {
                    Address = model.Address,
                    Latitude = model.Latitude,
                    Longitude = model.Longitude,
                    Name = model.Name,
                    ImageUrl = model.ImageUrl
                };
                var newShop = db.ShawaShops.Add(shop);
                db.SaveChanges();
                return RedirectToAction("Get/" + newShop.Id, "ShawaShop");
            }
            // TODO: redirect to error
            return View(model);
        }

        [HttpPost]
        public ActionResult AddReview(ReviewViewModel newReview)
        {
            if (ModelState.IsValid && Request.IsAuthenticated)
            {
                var userId = User.Identity.GetUserData().Id;
                db.Reviews.Add(new Review()
                {
                    Positive = newReview.Positive,
                    ShopId = newReview.ShopId,
                    Text = newReview.Text,
                    UserId = userId,
                });
                db.SaveChanges();
            }
            return RedirectToAction("Get/" + newReview.ShopId, "ShawaShop");
        }

        [HttpPost]
        public PartialViewResult SetScore(ScoreViewModel score)
        {
            if (!score.UserScore.HasValue || !db.ShawaShops.Any(shop => shop.Id == score.ShopId))
            {
                return PartialView("ShowAndAddScore", score);
            }
            int userId = User.Identity.GetUserData().Id;
            var existedScore = db.Scores.FirstOrDefault(score1 => score1.UserId == userId && score1.ShopId == score.ShopId);
            if (existedScore == null)
            {
                existedScore = db.Scores.Add(new Score()
                {
                    ShopId = score.ShopId,
                    UserId = userId,
                    Value = score.UserScore.Value
                });
            }
            else
            {
                existedScore.Value = score.UserScore.Value;
            }
            var selectedShop = existedScore.ShawaShop ?? db.ShawaShops.Get(existedScore.ShopId);
            var newScore = selectedShop.GetShowingScore(userId);
            db.SaveChanges();
            return PartialView("ShowAndAddScore", newScore);
        }

        [HttpPost]
        public PartialViewResult RemoveScore(int shopId)
        {
            int userId = User.Identity.GetUserData().Id;
            var existedScore = db.Scores.FirstOrDefault(score1 => score1.UserId == userId && score1.ShopId == shopId);
            ScoreViewModel score;
            if (existedScore != null)
            {
                db.Scores.Remove(existedScore);
                score = db.ShawaShops.Include(shop => shop.Scores).Get(shopId).GetShowingScore();
                db.SaveChanges();
            }
            else
            {
                score = db.ShawaShops.Get(shopId).GetShowingScore();
            }
            return PartialView("ShowAndAddScore", score);
        }

        public ActionResult Photos(int id = -1)
        {
            var shawaShop = db.ShawaShops.Get(id);
            if (shawaShop != null)
            {
                return View("Photos", shawaShop);
            }
            return HttpNotFound();
        }
    }
}