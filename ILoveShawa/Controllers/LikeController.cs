﻿using ILoveShawa.Models;
using ILoveShawa.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ILoveShawa.Controllers
{
    [Authorize]
    public class LikeController : Controller
    {
        private readonly ILoveShawaDbContext db;

        public LikeController(ILoveShawaDbContext dbContext)
        {
            db = dbContext;
        }

        private PartialViewResult SetLike(LikeViewModel likeResult)
        {
            int userId = User.Identity.GetUserData().Id;
            Like existingLike = db.Likes.FirstOrDefault(like => like.ReviewId == likeResult.ReviewId
                                                        && like.UserId == userId);
            if (existingLike == null)
            {
                existingLike = new Like
                {
                    UserId = userId,
                    ReviewId = likeResult.ReviewId
                };
                db.Likes.Add(existingLike);
            }
            else
            {
                db.Likes.Remove(existingLike);
            }
            Review review = db.Reviews.Get(existingLike.ReviewId);
            db.SaveChanges();
            return PartialView("Like", review.GetLikeViewModel(userId));
        }

        [HttpPost]
        public PartialViewResult SetLikeOn(LikeViewModel likeResult)
        {
            likeResult.Liked = true;
            return SetLike(likeResult);
        }

        [HttpPost]
        public PartialViewResult SetLikeOff(LikeViewModel likeResult)
        {
            likeResult.Liked = false;
            return SetLike(likeResult);
        }
    }
}