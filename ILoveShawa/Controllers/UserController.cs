﻿using ILoveShawa.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ILoveShawa.ViewModels;

namespace ILoveShawa.Controllers
{
    public class UserController : Controller
    {
        private readonly ILoveShawaDbContext db;

        public UserController(ILoveShawaDbContext db)
        {
            this.db = db;
        }

        [HttpGet]
        public ActionResult Get(int id = -1)
        {
            var user = db.Users.AsQueryable().Include(user1 => user1.Reviews).Get(id);
            if (user != null)
            {
                return View("User", user);
            }
            return HttpNotFound();
        }

        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View("Login");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel loginForm, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var checkedUser = db.Users.FirstOrDefault(user => user.Email == loginForm.Email);
                if (checkedUser != null && checkedUser.CheckPassword(loginForm.Password))
                {
                    Response.SetAuthCookie(checkedUser.Name, loginForm.RememberMe, checkedUser.GetUserData());
                    if (string.IsNullOrWhiteSpace(returnUrl))
                    {
                        return RedirectToAction("Index", "ShawaShop");
                    }
                    return Redirect(returnUrl);
                }

            }
            return View("Login", loginForm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff(string returnUrl)
        {
            FormsAuthentication.SignOut();
            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                return RedirectToAction("Index", "ShawaShop");
            }
            return Redirect(returnUrl);
        }


        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (db.Users.FirstOrDefault(user => user.Email == model.Email) != null)
                {
                    ModelState.AddModelError(string.Empty, "Пользователь с такой почтой уже существует");
                    return View(model);
                }
                var newUser = db.Users.Add(new User()
                {
                    Name = model.Name,
                    Password = ILoveShawa.Models.User.HashPassword(model.Password),
                    Email = model.Email,
                    ImageUrl = model.ImageUrl
                });
                db.SaveChanges();
                Response.SetAuthCookie(newUser.Name, false, newUser.GetUserData());
                return RedirectToAction("Index", "ShawaShop");
            }
            return View(model);
        }
    }
}
