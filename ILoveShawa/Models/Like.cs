namespace ILoveShawa.Models
{
    public class Like : BaseDBModel
    {
        public int UserId { get; set; }
        public int ReviewId { get; set; }

        public virtual Review Review { get; set; }
        public virtual User User { get; set; }
    }
}
