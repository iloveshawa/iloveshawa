using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ILoveShawa.Models
{
    public class Image : BaseDBModel
    {
        public string Url { get; set; }
        public int ShopId { get; set; }

        public virtual ShawaShop ShawaShop { get; set; }
    }
}
