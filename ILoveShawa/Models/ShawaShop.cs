using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using ILoveShawa.ViewModels;

namespace ILoveShawa.Models
{
    public class ShawaShop : BaseDBModel
    {
        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string ImageUrl { get; set; }
        public string Address { get; set; }

        public virtual ICollection<Image> Images { get; set; }

        public virtual ICollection<Review> Reviews { get; set; }

        public virtual ICollection<Score> Scores { get; set; }

        [NotMapped]
        public string AvatarUrl
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(ImageUrl))
                {
                    return ImageUrl;
                }
                if (Images.Count > 0)
                {
                    return Images.First().Url;
                }
                return "/Content/Images/shawashopavatarplacement.png";
            }
        }

        public ScoreViewModel GetShowingScore()
        {
            return new ScoreViewModel()
            {
                ScoreCount = Scores?.Count ?? 0,
                AverageScore = Scores != null && Scores.Any() ? Scores.Average(score => score.Value) : 0f,
                UserScore = null,
                ShopId = Id
            };
        }

        public ScoreViewModel GetShowingScore(int userId)
        {
            var result = GetShowingScore();
            var userScore = Scores.FirstOrDefault(score => score.UserId == userId);
            if (userScore != null)
            {
                result.UserScore = userScore.Value;
            }
            return result;
        }
    }
}
