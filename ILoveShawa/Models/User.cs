using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography;
using System.Text;

namespace ILoveShawa.Models
{
    public class User : BaseDBModel
    {
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        [Index("EmailIndex", IsUnique = true)]
        public string Email { get; set; }
        public string Password { get; set; }

        public virtual ICollection<Like> Likes { get; set; }

        public virtual ICollection<Review> Reviews { get; set; }

        public virtual ICollection<Score> Scores { get; set; }

        public bool CheckPassword(string password)
        {
            return Password.Equals(HashPassword(password), StringComparison.InvariantCultureIgnoreCase);
        }

        public static string HashPassword(string password)
        {
            HashAlgorithm hasher = new SHA256CryptoServiceProvider();
            var data = Encoding.ASCII.GetBytes(password);
            var hashedData = hasher.ComputeHash(data);
            return Convert.ToBase64String(hashedData);
        }

        public UserData GetUserData()
        {
            return new UserData { Id = Id, Email = Email, ImageUrl = ImageUrl, Name = Name };
        }
    }

    public class UserData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string Email { get; set; }
    }
}
