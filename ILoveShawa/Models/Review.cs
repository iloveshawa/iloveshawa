using ILoveShawa.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace ILoveShawa.Models
{
    public class Review : BaseDBModel
    {
        public string Text { get; set; }
        public bool Positive { get; set; }
        public int UserId { get; set; }
        public int ShopId { get; set; }

        public virtual ICollection<Like> Likes { get; set; }

        public virtual ShawaShop ShawaShop { get; set; }

        public virtual User User { get; set; }

        public LikeViewModel GetLikeViewModel(int userId = -1)
        {
            bool liked = false;
            foreach (Like like in Likes)
            {
                if (like.UserId == userId)
                {
                    liked = true;
                    break;
                }
            }
            return new LikeViewModel
            {
                ReviewId = Id,
                Liked = liked,
                Count = Likes.Count
            };
        }
    }
}
