namespace ILoveShawa.Models
{
    public class Score : BaseDBModel
    {
        public int Value { get; set; }
        public int UserId { get; set; }
        public int ShopId { get; set; }

        public virtual ShawaShop ShawaShop { get; set; }
        public virtual User User { get; set; }
    }
}
