﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using Microsoft.AspNet.Identity;

namespace ILoveShawa.Models
{
    public abstract class BaseDBModel
    {
        public int Id { get; set; }
    }

    public static class DbHelper
    {
        public static T Get<T>(this IEnumerable<T> enumerable, int id) where T : BaseDBModel
        {
            return enumerable.FirstOrDefault(arg => arg.Id == id);
        }

        public static int SetAuthCookie<T>(this HttpResponseBase responseBase, string name, bool rememberMe, T userData)
        {
            FormsAuthentication.SetAuthCookie(name, rememberMe);
            var cookie = FormsAuthentication.GetAuthCookie(name, rememberMe);
            var ticket = FormsAuthentication.Decrypt(cookie.Value);

            var newTicket = new FormsAuthenticationTicket(ticket.Version, ticket.Name, ticket.IssueDate, ticket.Expiration,
                ticket.IsPersistent, new JavaScriptSerializer().Serialize(userData), ticket.CookiePath);
            var encTicket = FormsAuthentication.Encrypt(newTicket);
            
            cookie.Value = encTicket;

            responseBase.Cookies.Add(cookie);

            return encTicket.Length;
        }

        public static UserData GetUserData(this IIdentity identity)
        {
            if (identity == null)
                throw new ArgumentNullException("identity");
            FormsIdentity identity1 = identity as FormsIdentity;
            if (identity1 != null)
                return new JavaScriptSerializer().Deserialize<UserData>(identity1.Ticket.UserData);
            return null;
        }
    }
}